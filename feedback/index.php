<?php
require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php';
$APPLICATION->SetTitle('Обратная связь')
?>
<?$APPLICATION->IncludeComponent(
    "bitrix:iblock.element.add.form",
    "aloe_feedback",
    array(
        "SEF_MODE" => "Y",
        "IBLOCK_TYPE" => "content",
        "IBLOCK_ID" => "3",
        "PROPERTY_CODES" => array(
            0 => "1",
            1 => "2",
            2 => "NAME",
        ),
        "PROPERTY_CODES_REQUIRED" => array(
            0 => "2",
            1 => "NAME",
        ),
        "GROUPS" => array(
            0 => "2",
        ),
        "STATUS_NEW" => "ANY",
        "STATUS" => "INACTIVE",
        "LIST_URL" => "",
        "ELEMENT_ASSOC" => "PROPERTY_ID",
        "ELEMENT_ASSOC_PROPERTY" => "3",
        "MAX_USER_ENTRIES" => "100000",
        "MAX_LEVELS" => "100000",
        "LEVEL_LAST" => "Y",
        "USE_CAPTCHA" => "Y",
        "USER_MESSAGE_EDIT" => "",
        "USER_MESSAGE_ADD" => "",
        "DEFAULT_INPUT_SIZE" => "30",
        "RESIZE_IMAGES" => "Y",
        "MAX_FILE_SIZE" => "0",
        "PREVIEW_TEXT_USE_HTML_EDITOR" => "Y",
        "DETAIL_TEXT_USE_HTML_EDITOR" => "Y",
        "CUSTOM_TITLE_NAME" => "Ваше имя",
        "SEF_FOLDER" => "/feedback/",
    ),
    false
);?>
<?php
require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php';
