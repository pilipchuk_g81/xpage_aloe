<?php

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Loader;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (empty($_GET['DELAY_ID']) === false) {
    if (Loader::includeModule('highloadblock')) {
        $hlblock = HighloadBlockTable::getById(FEEDBACK_DELAY_HLBLOCK_ID)->fetch();
        $entity = HighloadBlockTable::compileEntity($hlblock);
        $className = $entity->getDataClass();

        $rsDelayedMessages = $className::getList([
            'filter' => [
                'UF_DELAY_ID' => $_GET['DELAY_ID'],
            ],
            'select' => ['*'],
        ]);

        $rwDelayedMessage = $rsDelayedMessages->fetch();
    }
}
?>

<?php if (empty($rwDelayedMessage) === false) { ?>

    <script>
        var inputName = document.querySelector('input[name="PROPERTY[NAME][0]"]');
        inputName.value = '<?= ((!empty($rwDelayedMessage['UF_NAME'])) ? $rwDelayedMessage['UF_NAME'] : '') ?>';
        var emailInput = document.querySelector('input[name="PROPERTY[<?= EMAIL_PROP_ID ?>][0]"]');
        emailInput.value = '<?= ((!empty($rwDelayedMessage['UF_EMAIL'])) ? $rwDelayedMessage['UF_EMAIL'] : '') ?>';
        var phoneInput = document.querySelector('input[name="PROPERTY[<?= PHONE_PROP_ID ?>][0]"]');
        phoneInput.value = '<?= ((!empty($rwDelayedMessage['UF_PHONE'])) ? $rwDelayedMessage['UF_PHONE'] : '') ?>';
    </script>

<?php } ?>
