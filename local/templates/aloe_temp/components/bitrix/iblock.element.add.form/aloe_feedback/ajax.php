<?php

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Loader;

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);

require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

$result = ['status' => 'OK'];
$delayId = $_POST['sessid'] . uniqid();

$arFields = [];
$arFields['UF_NAME'] = (!empty($_POST['PROPERTY']['NAME'])) ? $_POST['PROPERTY']['NAME'][0] : '';
$arFields['UF_EMAIL'] = (!empty($_POST['PROPERTY'][EMAIL_PROP_ID])) ? $_POST['PROPERTY'][EMAIL_PROP_ID][0] : '';
$arFields['UF_PHONE'] = (!empty($_POST['PROPERTY'][PHONE_PROP_ID])) ? $_POST['PROPERTY'][PHONE_PROP_ID][0] : '';
$arFields['UF_DELAY_ID'] = $delayId;

if (true === empty($arFields['UF_EMAIL'])) {
    $result = [
        'status' => 'ERROR',
        'message' => 'Email is empty',
    ];
} else {
    if (Loader::includeModule('highloadblock')) {
        $hlblock = HighloadBlockTable::getById(FEEDBACK_DELAY_HLBLOCK_ID)->fetch();
        $entity = HighloadBlockTable::compileEntity($hlblock);
        $className = $entity->getDataClass();

        $className::add($arFields);

        \Bitrix\Main\Mail\Event::send([
            "EVENT_NAME" => "FEEDBACK_DELAY",
            "LID" => SITE_ID,
            "C_FIELDS" => [
                "DELAY_URL" => ($_SERVER['HTTPS']) ? 'https' : 'http' . '://'
                    . $_SERVER['SERVER_NAME']
                    . parse_url($_SERVER["HTTP_REFERER"], PHP_URL_PATH)
                    . "?DELAY_ID={$delayId}",
                "EMAIL_TO" => trim($arFields['UF_EMAIL']),
            ],
        ]);
    }
}

echo json_encode($result);

require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_after.php';
