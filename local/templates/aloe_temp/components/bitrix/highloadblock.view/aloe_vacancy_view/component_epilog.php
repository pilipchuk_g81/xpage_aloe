<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */
global $APPLICATION;

if (empty($arResult['row']['UF_DESCRIPTION']) === false) {
    $metaDesc = TruncateText(strip_tags($arResult['row']['UF_DESCRIPTION']), 150);
    $APPLICATION->SetPageProperty('description', $metaDesc);
}
