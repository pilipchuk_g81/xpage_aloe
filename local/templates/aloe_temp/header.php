<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title><?$APPLICATION->ShowTitle()?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" />
    <?$APPLICATION->ShowHead();?>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,700&amp;amp;subset=cyrillic" rel="stylesheet"/>
    <?php
    $asset = \Bitrix\Main\Page\Asset::getInstance();
    $asset->addCss(SITE_TEMPLATE_PATH . '/dist/styles.min.css');
    ?>
</head>
<body class="inner loading">
<div id="page-wr">
    <div id="panel"><?php $APPLICATION->ShowPanel(); ?></div>
    <div id="content">
        <header class="head">
            <div class="head__left">
                <div class="head-search">
                    <div class="search">
                        <form>
                            <div class="search__wrap">
                                <input type="text" placeholder="Поиск" class="search__input"/>
                                <input type="submit" value="" class="search__submit"/>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="head-contacts"><a href="#" class="head-contacts__link">Контакты</a></div>
            </div>
            <div class="head__logo"><a href="#" class="logo"><img src="<?= SITE_TEMPLATE_PATH ?>/img/logo.png" alt="" class="logo__img"/></a></div>
            <div class="head__right">
                <div class="head-links"><a href="#" class="head-links__link head-links__link--favorite"></a><a data-count="3" href="#" class="head-links__link head-links__link--cart"></a><a href="#" class="head-links__link head-links__link--lc"></a></div>
            </div>
            <div class="head__burger">
                <div class="burger"><span></span></div>
            </div>
        </header>
        <div class="wrapper">
            <nav class="head__menu">
                <ul class="menu">
                    <li class="menu__el"><a href="/feedback/" class="menu__link">Обратная связь</a></li>
                    <li class="menu__el"><a href="/vacancy/" class="menu__link">Вакансии</a></li>
                </ul>
            </nav>
        </div>
        <div class="content">
