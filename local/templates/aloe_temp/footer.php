<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
    </div>
    </div>
    <footer class="footer">
        <div class="wrapper">
            <div class="footer-line footer-line--top">
                <nav class="footer__menu">
                    <ul class="submenu__list">
                        <li class="submenu__list-el"><a href="#" class="submenu__list-link"> Уход за лицом</a></li>
                        <li class="submenu__list-el"><a href="#" class="submenu__list-link">уход за телом</a></li>
                        <li class="submenu__list-el"><a href="#" class="submenu__list-link">макияж</a></li>
                        <li class="submenu__list-el"><a href="#" class="submenu__list-link">для волос</a></li>
                    </ul>
                    <ul class="submenu__list">
                        <li class="submenu__list-el"><a href="#" class="submenu__list-link">Наборы</a></li>
                        <li class="submenu__list-el"><a href="#" class="submenu__list-link">Новинки</a></li>
                        <li class="submenu__list-el"><a href="#" class="submenu__list-link">Бестселлеры</a></li>
                        <li class="submenu__list-el"><a href="#" class="submenu__list-link">Бренды</a></li>
                    </ul>
                    <ul class="submenu__list">
                        <li class="submenu__list-el"><a href="#" class="submenu__list-link">О нас</a></li>
                        <li class="submenu__list-el"><a href="#" class="submenu__list-link">Контакты</a></li>
                        <li class="submenu__list-el"><a href="#" class="submenu__list-link">Новости</a></li>
                        <li class="submenu__list-el"><a href="#" class="submenu__list-link">Обзоры</a></li>
                    </ul>
                </nav>
                <div class="footer__phone">
                    <addres class="footer-phone"><a href="tel: 8 800 900-00-00">8 800 900-00-00</a></addres><a href="#" class="footer-call fancybox">Перезвоните мне</a>
                </div>
                <div class="footer__check">
                    <div class="f-auth"><a href="#" class="f-auth__link">Подлинность</a></div>
                </div>
            </div>
            <div class="footer-line footer-line--bot">
                <div class="footer__copy">
                    <div class="copy">© 2017 ООО «ALOE smart»</div><a href="#">Политика конфидециальности</a>
                </div>
                <div class="footer__soc">
                    <div class="soc"><a href="#" style="background-image: url('<?= SITE_TEMPLATE_PATH ?>/img/ico-vk.png')" class="soc__link"></a><a href="#" style="background-image: url('<?= SITE_TEMPLATE_PATH ?>/img/ico-inst.png')" class="soc__link"></a><a href="#" style="background-image: url('<?= SITE_TEMPLATE_PATH ?>/img/ico-fb.png')" class="soc__link"></a></div>
                </div>
                <div class="footer__dev"><a href="http://xpage.ru" traget="_blank" class="dev"><span class="dev__text">Сделано в </span><img src="<?= SITE_TEMPLATE_PATH ?>img/ico-dev.png" alt="" class="dev__img"/></a></div>
            </div>
        </div>
    </footer>
    </div>
<?php
$asset = \Bitrix\Main\Page\Asset::getInstance();
$asset->addJs(SITE_TEMPLATE_PATH . '/dist/vendor.js');
$asset->addJs(SITE_TEMPLATE_PATH . '/dist/scripts.js', true);
?>
</body>
</html>
