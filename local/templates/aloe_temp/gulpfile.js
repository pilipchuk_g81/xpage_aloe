var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var babel = require('gulp-babel');
var uglify = require('gulp-uglify');
var merge = require('merge-stream');
var plumber = require('gulp-plumber');
const webpack = require('webpack-stream');
const path = require('path');
const argv = require('yargs').argv;
const webpackRules = {
    rules: [
        {
            test: /\.(js|jsx|es6)$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                query: {
                    "plugins": [],
                    "presets": ['env', 'es2015', 'stage-2'],
                },
            },

        }
    ]
};

var gulpRootFolder = process.cwd();

gulp.task('css', function() {
    let theme = gulp.src([
        'css/common.css',
        'css/jquery.fancybox.css',
        'css/ui-slider.css',
        'css/jquery.jscrollpane.css',
        'css/fonts.css',
        'css/slick.css',
        'css/main.css',
        'css/forms.css',
        'css/main_data.css',
        'node_modules/notifier-js/dist/css/notifier.css',
    ]);

    let custom = gulp.src([
        'sass/main.scss',
    ])
        .pipe(plumber())
        // .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded',
            includePaths: [
                'sass/',
            ]
        }).on('error', gutil.log));

    return merge(theme, custom)
        .pipe(concat('styles.css'))
        // .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist'))
        .pipe(concat('styles.min.css'))
        .pipe(cleanCSS({
            rebaseTo: `${gulpRootFolder}/dist/`,
        }))
        .pipe(gulp.dest('dist'));
});

gulp.task('vendor-js', function () {
    return gulp.src([
        'js/jquery.min.js',
        'js/jquery.mousewheel.js',
        'js/jquery.jscrollpane.min.js',
        'js/jquery.ui-slider.js',
        'js/slick.js',
        'js/jquery.fancybox.js',
        'js/common.js',
    ])
        .pipe(concat('vendor.js'), {
            newLine:';' // the newline is needed in case the file ends with a line comment, the semi-colon is needed if the last statement wasn't terminated
        })
        // .pipe(sourcemaps.init())
        // .pipe(babel({
        //     presets: ['env'],
        //     compact: false
        // }))
        .pipe(gulp.dest('dist'))
        .pipe(concat('vendor.min.js'), {
            newLine:';' // the newline is needed in case the file ends with a line comment, the semi-colon is needed if the last statement wasn't terminated
        })
        .pipe(uglify())
        // .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist'))
});

gulp.task('js', function() {
    let custom = gulp.src([
        'js/custom.js',
    ]);
    return custom
        .pipe(plumber())
        // .pipe(sourcemaps.init())
        .pipe(webpack({
            mode: (argv.prod) ? 'production' : 'development',
            devtool: (argv.prod) ? false : 'source-map',
            module: webpackRules,
            // entry: path.resolve(__dirname, 'js/order_ajax.js'),
            output: {
                filename: 'scripts.js',
                // path: path.resolve(__dirname, 'dist')
            }
        }))
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', function() {
    gulp.watch('sass/*.scss', ['css']);
    gulp.watch('js/*.js', ['js']);
});

gulp.task('default', ['css', 'js']);
