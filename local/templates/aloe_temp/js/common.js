"use strict";

if (window.frameCacheVars !== undefined) BX.addCustomEvent("onFrameDataReceived", function (json) {
	loadScripts();
});else if (typeof BX != "undefined") BX.ready(function () {
	loadScripts();
});else $(function () {
	loadScripts();
});

$(window).on("load", function () {
	loaded();
});

var loaded = function loaded() {
	$("body").addClass("loaded").removeClass("loading");
},
    loadScripts = function loadScripts() {
	$("body").on("change", ".forms__input--file", function () {
		var $this = $(this),
		    name = $this[0].files[0].name;

		$this.nextAll(".forms__input--file-support").val(name);
	});
	/**Адаптив*/

	// $(".submenu__list").each((i, el) => {
	// 	var $this = $(el);

	// 	$this.closest(".submenu__col").addClass("js__have-sub");
	// });

	$(".burger").click(function () {

		$(this).toggleClass("js-active");

		$("body").toggleClass("js-menu-opened");
	});

	$(".head__menu").append("<div class='head__menu-footer js__moved-phone'></div>");

	$(".js__moved-phone").append($('.footer-phone').clone()).append($('.footer-call').clone());

	var adaptiveLinks = [];

	adaptiveLinks.push({
		name: "Личный кабинет",
		link: $(".head-links__link--lc").attr("href"),
		postfix: "lc"
	});

	adaptiveLinks.push({
		name: "Избранное",
		link: $(".head-links__link--favorite").attr("href"),
		postfix: "favorite"
	});

	for (var key in adaptiveLinks) {
		var link = adaptiveLinks[key];
		$(".menu").prepend("<li class='menu__el js__moved-head-link js__moved-head-link--" + link.postfix + "'>\
				<a class='menu__link' href='" + link.link + "'>" + link.name + "</a></li>");
	}

	$(".menu__el.sub .menu__link").click(function (e) {

		if ($(window).width() <= 1000) {
			var $this = $(this);

			$(".js-submenu-opened").each(function (i, el) {
				var $this = $(el);

				if ($(e.target).is($this) || $this.has(e.target).length) return;

				$this.find(".submenu").slideUp(300);

				$this.removeClass("js-submenu-opened");
			});

			$this.closest(".sub").toggleClass("js-submenu-opened");

			$this.next(".submenu").slideToggle(300);

			return false;
		}
	});

	$(".catalog-filter__btn").click(function () {
		var $this = $(this);
		var tmpText = $this.text();

		$this.text($this.attr("data-toggle-text"));
		$this.attr("data-toggle-text", tmpText);

		$(".catalog-filter").slideToggle(300);
	});

	/**!Адаптив*/

	$(".tovar-info .tabs-content[data-id=\"0\"] .text-page,\
		.tovar-desc__row-text.text-page").each(function (i, el) {
		var $this = $(el);
		text = $this.text();

		// let splitedText = text.split(new RegExp(/.[А-Я]/));
		// let count = splitedText.length;

		var anotherSplitedText = text.split(new RegExp(/\n/));

		$this.text("");

		var tmpText = "",
		    isUlOpened = false;

		for (var _i in anotherSplitedText) {
			var abz = anotherSplitedText[_i];

			if (abz.match(new RegExp(/^-|• .*/))) {
				if (!isUlOpened) {
					tmpText += "<ul>";
					isUlOpened = true;
				}

				abz = abz.replace(/^-|• /, "");

				tmpText += "<li>" + abz + "</li>";
			} else {
				if (isUlOpened) {
					isUlOpened = false;
					tmpText += "</ul>";
				}

				tmpText += "<p>" + abz + "</p>";
			}
		}

		$this.html(tmpText);

		// console.log(anotherText);
	});

	$(".tovar-desc__row-text.text-page").each(function (i, el) {
		var $this = $(el),
		    $prev = $this.prev(".tovar-desc__row-title");

		if ($prev.text() + ":" == $this.find("p:eq(0)").text().replace(/^ /, "")) $this.find("p:eq(0)").remove();
	});

	$("body").on("click", ".forms-input-cont .rating__list:not(.js__voted) .rating__start", function () {
		var $this = $(this);
		var index = $this.index(),
		    $rating = $this.closest("ul");

		$rating.find("li").each(function (i, el) {
			var $this = $(el);

			if ($this.index() <= index) $this.addClass("active");
		});

		// $rating.addClass("js__voted");

		$rating.next("input").val((index + 1) / 2);
	});

	$(".text-page table").each(function (i, el) {
		var $this = $(el);

		$this.wrap("<div class='table-wrap'></div>");
	});

	$(".new-slider").each(function (i, el) {
		var $this = $(el);

		$this.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			slide: ".new-slider__slide",
			fade: true,
			appendArrows: $this.closest(".new").find(".new__title-cont")
		});
	});

	$(".shop-slider").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		slide: ".shop-slider__slide"
	});

	$(".share").click(function () {
		var $this = $(this);

		$this.closest("div").find(".tovar__share-yashare").toggleClass("js__visible");
	});

	$("body").on("click", function (e) {
		if (!$(e.target).is($(".tovar__share")) && !$(".tovar__share").has(e.target).length) $(".tovar__share-yashare").removeClass("js__visible");
	});

	$(".filter-checks__list").jScrollPane({
		autoReinitialise: true
	});

	$(".categories__one-title").click(function () {
		var $this = $(this);

		$this.next(".categories__one-main").slideToggle(300);

		$this.closest(".categories__one").toggleClass("closed");
	});

	$(".filter__one-title-cont").click(function () {
		var $this = $(this);

		$this.next(".filter__one-block").slideToggle(300);
	});

	$(".fancybox").fancybox({
		beforeShow: function beforeShow() {
			$("body").addClass("fancy-active");
		},
		afterClose: function afterClose() {
			$("body").removeClass("fancy-active");
		}
		// afterShow: () => {
		// 	//alert()
		// 	if (!$(window).width() > 767)
		// 		setTimeout(()=>{
		// 			$(".fancybox-wrap").css({
		// 				top: $(".fancybox-overlay").offset().top
		// 			})
		// 		}, 300);

		//   // document.addEventListener('touchmove', addSafariCutch);

		// }
	});

	$(".price-filter").each(function (i, el) {
		var $this = $(el);

		var $range = $this.find(".range"),
		    $min = $this.find("input[data-min]"),
		    $max = $this.find("input[data-max]");

		var min = parseInt($min.attr("data-min")),
		    max = parseInt($max.attr("data-max"));

		$range.slider({
			animate: "normal",
			min: min,
			max: max,
			values: [parseInt($min.val()), parseInt($max.val())],
			range: true,
			step: 10,
			slide: function slide(e, ui) {
				$min.val(parseInt(ui.values[0]));
				$max.val(parseInt(ui.values[1]));
			}
		});

		$this.find(".price-filter__input").on("keyup", function () {
			var id = void 0,
			    val = void 0;

			if ($(this).attr("data-min")) {
				id = 0;
				val = +$(this).val() < min ? min : +$(this).val();
			} else {
				id = 1;
				val = +$(this).val() > max ? max : +$(this).val();
			}

			$range.slider("values", id, parseInt(val));
		});
	});

	$(".main-slider").slick({
		slidesToScroll: 1,
		slidesToShow: 1,
		slide: ".main-slider__slide",
		autoplay: true,
		fade: true,
		autoplaySpeed: 4000,
		responsive: [{
			breakpoint: 1000,
			settings: {
				arrows: false,
				dots: true
			}
		}]
	});

	$(".bestseller--slider").each(function (i, el) {
		var $this = $(el);

		$this.slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			appendArrows: $this.closest(".bestsellers__cont").find(".new__title-cont"),
			responsive: [{
				breakpoint: 1000,
				settings: {
					slidesToShow: 2
				}
			}, {
				breakpoint: 660,
				settings: {
					slidesToShow: 1
				}
			}]
		});
	});

	$(".partners").slick({
		slidesToShow: 6,
		slidesToScroll: 1,
		slide: ".partners__slide",
		responsive: [{
			breakpoint: 1500,
			settings: {
				slidesToShow: 4
			}
		}, {
			breakpoint: 1000,
			settings: {
				slidesToShow: 3
			}
		}]
	});

	$(".tovar-big-slider").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		slide: ".tovar-big-slider__slide",
		asNavFor: ".tovar-smallslider",
		fade: true
	});

	$(".tovar-smallslider").slick({
		slidesToScroll: 1,
		slidesToShow: 3,
		vertical: true,
		verticalSwiping: true,
		slide: ".tovar-smallslider__slide",
		arrows: false,
		asNavFor: ".tovar-big-slider",
		focusOnSelect: true,
		responsive: [{
			breakpoint: 1200,
			settings: {
				vertical: false,
				verticalSwiping: false,
				slidesToShow: 2
			}
		}, {
			breakpoint: 1000,
			settings: {
				vertical: false,
				verticalSwiping: false,
				slidesToShow: 3
			}
		}]
	});

	$(".tabs-tab").click(function () {
		var $this = $(this);

		if ($this.hasClass("active")) return;

		var id = $this.attr("data-id"),
		    $parent = $this.closest(".tabs");

		$parent.find(".tabs-tab.active, .tabs-content.active").removeClass("active");

		$this.addClass("active");
		$parent.find(".tabs-content[data-id='" + id + "']").addClass("active");
	});
};

// let startY = 0, fancyScroll = 0;
// document.addEventListener('touchstart', function(e) {
//     startY = e.touches[0].screenY;
//      fancyScroll = $(".fancybox-inner").scrollTop();
// });

function addSafariCutch(e) {
	// if (!$("html").hasClass("bx-ios"))
	// 	return

	// let amountMovedY = e.touches[0].screenY - startY;

	// e.preventDefault();

	// amountMovedY *= -1;

	// $(".fancybox-inner").scrollTop(fancyScroll+amountMovedY);
}
//# sourceMappingURL=common.js.map
