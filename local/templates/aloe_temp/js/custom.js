require('es6-promise').polyfill();
require('isomorphic-fetch');

import notifier from 'notifier-js';

function ready(fn) {
    if (document.readyState != 'loading') {
        fn();
    } else if (document.addEventListener) {
        document.addEventListener('DOMContentLoaded', fn);
    } else {
        document.attachEvent('onreadystatechange', function () {
            if (document.readyState != 'loading') {
                fn();
            }
        });
    }
}

function initCustom()
{
    const feedbackDelayBtn = document.getElementById('iblock-form-delay');
    if (feedbackDelayBtn) {
        feedbackDelayBtn.addEventListener('click', (event) => {
            event.preventDefault();
            const targetForm = event.target.form || null;
            if (targetForm) {
                let bodyData = new FormData();
                Array.from(targetForm.elements).forEach((item) => {
                    if (item.tagName === 'INPUT') {
                        bodyData.append(item.name, item.value);
                    }
                });
                fetch(event.target.dataset.url, {
                    method: 'post',
                    body: bodyData,
                })
                    .then((response) => {
                        return response.json();
                    })
                    .then((response) => {
                        if (response.status) {
                            if (response.status === 'OK') {
                                notifier.show('Сообщение отложено', '', 'success', '', 3000);
                            } else if (response.status === 'ERROR') {
                                notifier.show(
                                    'Ошибка',
                                    response.message || '',
                                    'danger',
                                    '',
                                    3000
                                );
                            }
                        }
                    })
                    .catch((error) => {
                        console.error(error);
                    })
            }
        });
    }
}

ready(initCustom);
