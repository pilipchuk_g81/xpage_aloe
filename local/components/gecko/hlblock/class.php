<?php

use Bitrix\Highloadblock\HighloadBlockTable;
use \Bitrix\Main;
use Bitrix\Main\Context;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Error;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CUser $USER
 * @global CMain $APPLICATION
 */

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('highloadblock')) {
    ShowError('Highloadblock модуль не установлен');
    return;
}

class HighloadComplex extends CBitrixComponent
{
    public function onPrepareComponentParams($params)
    {
        if (isset($params["USE_FILTER"]) && $params["USE_FILTER"] == "Y") {
            $params["FILTER_NAME"] = trim($params["FILTER_NAME"]);
            if ($params["FILTER_NAME"] === '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $params["FILTER_NAME"])) {
                $params["FILTER_NAME"] = "arrFilter";
            }
        } else {
            $params["FILTER_NAME"] = "";
        }

        return $params;
    }

    public function executeComponent()
    {
        global $APPLICATION;

        $arDefaultUrlTemplates404 = array(
            "sections" => "",
            "element" => "#ELEMENT_CODE#/",
        );

        $arDefaultVariableAliases404 = array();

        $arDefaultVariableAliases = array();

        $arComponentVariables = array(
            "ELEMENT_ID",
            "ELEMENT_CODE",
        );

        $arVariables = array();

        $engine = new CComponentEngine($this);
        $arUrlTemplates = CComponentEngine::makeComponentUrlTemplates($arDefaultUrlTemplates404, $this->arParams["SEF_URL_TEMPLATES"]);
        $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases404, $this->arParams["VARIABLE_ALIASES"]);

        $componentPage = $engine->guessComponentPath(
            $this->arParams["SEF_FOLDER"],
            $arUrlTemplates,
            $arVariables
        );

        $b404 = false;
        if (!$componentPage) {
            $componentPage = "sections";
            $b404 = true;
        }

        if ($componentPage === 'element') {
            $hlblock = HighloadBlockTable::getById($this->arParams['HLBLOCK_ID'])->fetch();
            $entity = HighloadBlockTable::compileEntity($hlblock);
            $className = $entity->getDataClass();

            $rsElement = $className::getList([
                'limit' => 1,
                'select' => ['ID'],
                'filter' => [
                    '=UF_CODE' => $arVariables['ELEMENT_CODE'],
                ]
            ]);

            $rwElement = $rsElement->fetch();
            if (empty($rwElement) === false) {
                $arVariables['ELEMENT_ID'] = $rwElement['ID'];
            } else {
                $b404 = true;
            }
        }

        if ($b404) {
            $folder404 = str_replace("\\", "/", $this->arParams["SEF_FOLDER"]);
            if ($folder404 != "/") {
                $folder404 = "/" . trim($folder404, "/ \t\n\r\0\x0B") . "/";
            }
            if (substr($folder404, -1) == "/") {
                $folder404 .= "index.php";
            }

            if ($folder404 != $APPLICATION->GetCurPage(true)) {
                self::process404(
                    ""
                    , ($this->arParams["SET_STATUS_404"] === "Y")
                    , ($this->arParams["SET_STATUS_404"] === "Y")
                    , ($this->arParams["SHOW_404"] === "Y")
                    , $this->arParams["FILE_404"]
                );
            }
        }

        CComponentEngine::initComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
        $this->arResult = array(
            "FOLDER" => $this->arParams["SEF_FOLDER"],
            "URL_TEMPLATES" => $arUrlTemplates,
            "VARIABLES" => $arVariables,
            "ALIASES" => $arVariableAliases
        );

        $this->IncludeComponentTemplate($componentPage);
    }

    public static function process404(
        $message = "",
        $defineConstant = true,
        $setStatus = true,
        $showPage = false,
        $pageFile = ""
    )
    {
        /** @global \CMain $APPLICATION */
        global $APPLICATION;

        if ($message <> "") {
            $APPLICATION->includeComponent(
                "bitrix:system.show_message",
                ".default",
                array(
                    "MESSAGE" => $message,
                    "STYLE" => "errortext",
                ),
                null,
                array(
                    "HIDE_ICONS" => "Y",
                )
            );
        }

        if ($defineConstant && !defined("ERROR_404")) {
            define("ERROR_404", "Y");
        }

        if ($setStatus) {
            \CHTTP::setStatus("404 Not Found");
        }

        if ($showPage) {
            if ($APPLICATION->RestartWorkarea()) {
                if (!defined("BX_URLREWRITE")) {
                    define("BX_URLREWRITE", true);
                }
                \Bitrix\Main\Page\Frame::setEnable(false);
                if ($pageFile) {
                    require(\Bitrix\Main\Application::getDocumentRoot() . rel2abs("/", "/" . $pageFile));
                } else {
                    require(\Bitrix\Main\Application::getDocumentRoot() . "/404.php");
                }
                die();
            }
        }
    }
}
