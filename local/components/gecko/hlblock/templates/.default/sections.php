<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

$APPLICATION->IncludeComponent(
    "bitrix:highloadblock.list",
    "aloe_vacancy_list",
    Array(
        "BLOCK_ID" => $arParams['HLBLOCK_ID'],
        "CHECK_PERMISSIONS" => "Y",
        "DETAIL_URL" => "/vacancy/#UF_CODE#/",
        "FILTER_NAME" => "myfilter",
        "PAGEN_ID" => "page",
        "ROWS_PER_PAGE" => "3"
    )
);
