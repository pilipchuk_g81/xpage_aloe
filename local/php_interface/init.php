<?php

const FEEDBACK_DELAY_HLBLOCK_ID = 1;
const EMAIL_PROP_ID = 2;
const PHONE_PROP_ID = 1;

\Bitrix\Main\EventManager::getInstance()->addEventHandlerCompatible(
    'iblock',
    'OnAfterIBlockElementAdd',
    function (&$arFields) {
        \Bitrix\Main\Mail\Event::send([
            "EVENT_NAME" => "FEEDBACK_SUBMIT",
            "LID" => SITE_ID,
            "C_FIELDS" => [
                "AUTHOR_NAME" => $arFields['NAME'],
                "AUTHOR_EMAIL" => $arFields['PROPERTY_VALUES'][EMAIL_PROP_ID],
                "AUTHOR_PHONE" => $arFields['PROPERTY_VALUES'][PHONE_PROP_ID],
            ],
        ]);
    }
);
