<?php
require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php';
$APPLICATION->SetTitle('Вакансии')
?>
<?$APPLICATION->IncludeComponent(
    "gecko:hlblock",
    "",
    Array(
        "HLBLOCK_ID" => "2",
        "SEF_FOLDER" => "/vacancy/",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "SET_TITLE" => "Y",
        "USE_FILTER" => "N",
        "FILTER_NAME" => "",
        "PAGE_ELEMENT_COUNT" => "3",
        "PAGER_TEMPLATE" => "arrows",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Вакансии",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_BASE_LINK_ENABLE" => "Y",
        "PAGER_BASE_LINK" => "",
        "PAGER_PARAMS_NAME" => "arrPager",
        "SET_STATUS_404" => "N",
        "SHOW_404" => "N",
        "MESSAGE_404" => "",
        "SEF_URL_TEMPLATES" => array(
            "sections" => "",
            "element" => "#ELEMENT_CODE#/",
        )
    ),
    false
);?>
<?php
require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php';
